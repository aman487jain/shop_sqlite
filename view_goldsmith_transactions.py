import datetime
import sqlite3
import tkinter as tk
import tkinter.ttk as ttk
from functools import partial
from tkinter import messagebox
from tkinter.font import Font


class CashEntry:
    def __init__(self, top_window):
        self.transactions_scrollbar = None
        self.opening_actual_pure_balance = None
        self.opening_pure_balance_label = None
        self.opening_actual_cash_balance = None
        self.opening_cash_balance_label = None
        self.opening_balance_frame = None
        self.transactions_tree = None
        self.goldsmith_details = None
        self.last_10_transactions_frame = None
        self.font = Font(family='TkHeadingFont', size=15)

        self.db = sqlite3.connect("shop.db", detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)

        self.cursor = self.db.cursor()
        self.root_window = tk.Toplevel(top_window)
        self.root_window.grab_set()
        self.root_window.title('VIEW TRANSACTIONS')
        width = self.root_window.winfo_screenwidth()
        height = self.root_window.winfo_screenheight()
        self.root_window.geometry(str(width - 100) + "x" + str(height - 100))

        self.cursor.execute('''SELECT * from goldsmiths''')
        result = self.cursor.fetchall()
        self.goldsmith_list = {x[1]: x[0] for x in result}
        self.lst = [x[1] for x in result]

        # creating Combobox
        self.root_window.option_add("*TCombobox*Listbox*Font", self.font)
        self.top_frame = tk.Frame(self.root_window, borderwidth=15)

        self.goldsmith_frame = tk.Frame(self.top_frame)
        self.goldsmith_frame.grid(column=0, row=0, sticky="news", padx=(10, 100))
        self.goldsmith_label = tk.Label(self.goldsmith_frame, font=('TkHeadingFont', 15), text="Goldsmith Name : ")
        self.goldsmith_label.pack(expand=True, side="left", anchor="e")
        self.goldsmith_combo_box = ttk.Combobox(self.goldsmith_frame, font=('TkHeadingFont', 15))
        self.goldsmith_combo_box['values'] = self.lst
        self.goldsmith_combo_box.bind("<<ComboboxSelected>>", self.display_all_fields)
        self.goldsmith_combo_box.bind('<KeyRelease>', self.update_goldsmith_list)
        self.goldsmith_combo_box.pack(side="left", expand=True)

        self.time_period = tk.IntVar()

        self.today_radio_button = tk.Radiobutton(self.top_frame, font=('TkHeadingFont', 15), value=1,
                                                 variable=self.time_period, height=3,
                                                 command=partial(self.display_all_fields, ""),
                                                 indicatoron=False, selectcolor='green', )
        self.today_radio_button.configure(text='Today')
        self.today_radio_button.grid(column=1, row=0, sticky='nsew')
        self.last_2_radio_button = tk.Radiobutton(self.top_frame, font=('TkHeadingFont', 15), value=2,
                                                  variable=self.time_period, height=3,
                                                  command=partial(self.display_all_fields, ""),
                                                  indicatoron=False, selectcolor='green', )
        self.last_2_radio_button.configure(text='2 Days')
        self.last_2_radio_button.grid(column=2, row=0, sticky='nsew')
        self.last_5_radio_button = tk.Radiobutton(self.top_frame, font=('TkHeadingFont', 15), value=5,
                                                  variable=self.time_period, height=3,
                                                  command=partial(self.display_all_fields, ""),
                                                  indicatoron=False, selectcolor='green', )
        self.last_5_radio_button.configure(text='5 Days')
        self.last_5_radio_button.grid(column=3, row=0, sticky='nsew')
        self.last_7_radio_button = tk.Radiobutton(self.top_frame, font=('TkHeadingFont', 15), value=7,
                                                  variable=self.time_period, height=3,
                                                  command=partial(self.display_all_fields, ""),
                                                  indicatoron=False, selectcolor='green', )
        self.last_7_radio_button.configure(text='7 Days')
        self.last_7_radio_button.grid(column=4, row=0, sticky='nsew')
        self.all_radio_button = tk.Radiobutton(self.top_frame, font=('TkHeadingFont', 15), value=99,
                                               variable=self.time_period, height=3,
                                               command=partial(self.display_all_fields, ""),
                                               indicatoron=False, selectcolor='green', )
        self.all_radio_button.configure(text='All')
        self.all_radio_button.grid(column=5, row=0, sticky='nsew')
        self.top_frame.rowconfigure('all', weight=1)
        self.top_frame.columnconfigure('all', weight=1)
        self.top_frame.configure(borderwidth='5', relief='raised')

        self.top_frame.pack(side="top", expand=False, fill="x")

    def display_all_fields(self, _):
        if self.last_10_transactions_frame is not None:
            self.last_10_transactions_frame.pack_forget()
            self.last_10_transactions_frame.destroy()
        if self.opening_balance_frame is not None:
            self.opening_balance_frame.pack_forget()
            self.opening_balance_frame.destroy()

        if self.time_period.get() not in [1, 2, 5, 7, 99]:
            messagebox.showerror("Error", "Select Date Range...", parent=self.root_window)
            return

        self.opening_balance_frame = tk.Frame(self.root_window, borderwidth=5, relief=tk.RAISED)
        self.opening_cash_balance_label = tk.Label(self.opening_balance_frame)
        self.opening_actual_cash_balance = tk.Label(self.opening_balance_frame)
        self.opening_pure_balance_label = tk.Label(self.opening_balance_frame)
        self.opening_actual_pure_balance = tk.Label(self.opening_balance_frame)
        self.opening_balance_frame.pack(side="top", expand=False, fill="x")
        self.opening_cash_balance_label.configure(justify='right', text='Cash Balance : ', anchor="e",
                                                  font=('TkHeadingFont', 15))
        self.opening_cash_balance_label.pack(expand=True, fill='x', side='left')
        self.opening_actual_cash_balance.configure(justify='left', text="", anchor="w",
                                                   font=('TkHeadingFont', 15))
        self.opening_actual_cash_balance.pack(expand=True, fill='x', side='left')
        self.opening_pure_balance_label.configure(justify='right', text='Pure Balance : ', anchor="e",
                                                  font=('TkHeadingFont', 15))
        self.opening_pure_balance_label.pack(expand=True, fill='x', side='left')
        self.opening_actual_pure_balance.configure(justify='left', text="", anchor="w",
                                                   font=('TkHeadingFont', 15))
        self.opening_actual_pure_balance.pack(expand=True, fill='x', side='left')
        self.opening_balance_frame.update()

        self.last_10_transactions_frame = tk.Frame(self.root_window, relief=tk.RAISED, borderwidth=5)
        self.last_10_transactions_frame.pack(side="top", expand=True, fill="both")
        self.transactions_tree = ttk.Treeview(self.last_10_transactions_frame)
        s = ttk.Style()
        s.theme_use('clam')
        s.configure('Treeview', rowheight=35)
        s.configure('Treeview.Heading', font=('TkHeadingFont', 15))

        self.transactions_tree.configure(columns=(
            "date", "name", "type", "desc", "amount", "gold_rate", "gross_weight", "purity", "purity_ded",
            "pure_weight"), show="headings")
        self.transactions_tree.heading('date', text='Date')
        self.transactions_tree.heading('name', text="goldsmith Name")
        self.transactions_tree.heading('type', text='Type')
        self.transactions_tree.heading('desc', text='Description')
        self.transactions_tree.heading("amount", text='Amount')
        self.transactions_tree.heading("gold_rate", text="Gold Rate")
        self.transactions_tree.heading("gross_weight", text="Gross Weight")
        self.transactions_tree.heading("purity", text="Purity")
        self.transactions_tree.heading("purity_ded", text="Purity Minus")
        self.transactions_tree.heading("pure_weight", text="Pure Weight")

        col_width = self.transactions_tree.winfo_width() // 12

        today = (datetime.datetime.now() + datetime.timedelta(days=1)).strftime("%Y-%m-%d")

        if self.time_period.get() == 1:
            end = (datetime.datetime.now() - datetime.timedelta(days=0)).strftime("%Y-%m-%d")
        elif self.time_period.get() == 2:
            end = (datetime.datetime.now() - datetime.timedelta(days=1)).strftime("%Y-%m-%d")
        elif self.time_period.get() == 5:
            end = (datetime.datetime.now() - datetime.timedelta(days=4)).strftime("%Y-%m-%d")
        elif self.time_period.get() == 7:
            end = (datetime.datetime.now() - datetime.timedelta(days=6)).strftime("%Y-%m-%d")
        else:
            end = (datetime.datetime.now() - datetime.timedelta(days=1000)).strftime("%Y-%m-%d")

        if self.goldsmith_combo_box.get() is None or self.goldsmith_combo_box.get().strip() == "":
            self.cursor.execute(
                "SELECT date, goldsmith_id, type, description, amount, gold_rate, gross_weight, purity, "
                "purity_ded, pure_weight FROM goldsmith_TRANSACTIONS WHERE DATE(date) BETWEEN '" +
                str(end) + "' AND '" + str(today) + "' ORDER BY date DESC")

        else:
            self.cursor.execute("SELECT * FROM goldsmithS WHERE NAME like '" + self.goldsmith_combo_box.get() + "'")
            self.goldsmith_details = self.cursor.fetchone()
            if self.goldsmith_details is None:
                self.cursor.execute(
                    "SELECT date, goldsmith_id, type, description, amount, gold_rate, gross_weight, purity, "
                    "purity_ded, pure_weight FROM goldsmith_TRANSACTIONS WHERE DATE(date) BETWEEN '" +
                    str(end) + "' AND '" + str(today) + "' ORDER BY date DESC")
            else:
                self.cursor.execute(
                    "SELECT PURE_BAL, CASH_BAL FROM goldsmithS WHERE ID = " + str(self.goldsmith_details[0]))
                bal_details = self.cursor.fetchone()
                self.opening_actual_cash_balance.configure(text=bal_details[1])
                self.opening_actual_pure_balance.configure(text=bal_details[0])
                self.cursor.execute(
                    "SELECT date, goldsmith_id, type, description, amount, gold_rate, gross_weight, purity, "
                    "purity_ded, pure_weight FROM goldsmith_TRANSACTIONS WHERE goldsmith_ID = " +
                    str(self.goldsmith_details[0]) + " AND DATE(date) BETWEEN '" + str(end) + "' AND '" +
                    str(today) + "'  ORDER BY date DESC LIMIT 10")

        result = self.cursor.fetchall()
        values = []
        for i in result:
            row = []
            for val in i:
                if not row:
                    val = val + datetime.timedelta(hours=0, minutes=0)
                    row.append(val.strftime("%d-%m-%y %H:%M"))
                elif len(row) == 1:
                    self.cursor.execute("SELECT NAME FROM goldsmithS WHERE ID = " + str(val))
                    row.append(self.cursor.fetchone()[0])
                elif val is None:
                    row.append("-----")
                else:
                    row.append(val)
            values.append(row)

        for value in values:
            self.transactions_tree.insert('', tk.END, values=value, tags="columns")
        self.transactions_tree.tag_configure('columns', font=("TkHeadingFont", 15))
        self.transactions_tree.column('date', anchor="center", width=col_width * 1)
        self.transactions_tree.column('name', anchor="center", width=col_width * 2)

        self.transactions_tree.column('type', anchor="center", width=col_width * 1)
        self.transactions_tree.column('desc', anchor="center", width=col_width * 2)
        self.transactions_tree.column("amount", anchor="center", width=col_width * 1)
        self.transactions_tree.column("gold_rate", anchor="center", width=col_width * 1)
        self.transactions_tree.column("gross_weight", anchor="center", width=col_width * 1)
        self.transactions_tree.column("purity", anchor="center", width=col_width * 1)
        self.transactions_tree.column("purity_ded", anchor="center", width=col_width * 1)
        self.transactions_tree.column("pure_weight", anchor="center", width=col_width * 1)

        self.transactions_tree.pack(side="left", expand=True, fill="both")

        self.transactions_scrollbar = tk.Scrollbar(self.last_10_transactions_frame)
        self.transactions_scrollbar.configure(orient='vertical', width=15)
        self.transactions_scrollbar.pack(expand=False, fill='y', side='right')
        self.transactions_tree.config(yscrollcommand=self.transactions_scrollbar.set)

        self.transactions_scrollbar.config(command=self.transactions_tree.yview)

    def update_goldsmith_list(self, event):
        value = event.widget.get()

        if value == '':
            self.goldsmith_combo_box['values'] = self.lst
        else:
            data = []
            for item in self.lst:
                if value.lower() in item.lower():
                    data.append(item)

            self.goldsmith_combo_box['values'] = data


def view_goldsmith_transactions(main_window, _):
    cash_entry_window = CashEntry(main_window)
    cash_entry_window.root_window.mainloop()
