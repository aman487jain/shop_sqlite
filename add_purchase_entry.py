import sqlite3
import tkinter.ttk as ttk
import datetime
import tkinter as tk
from functools import partial
from tkinter import messagebox
from tkinter.font import Font


class PurchaseEntry:
    def __init__(self, top_window):
        self.purity_ded = None
        self.purity_ded_label = None
        self.goldsmith_details = None
        self.font = Font(family='TkHeadingFont', size=15)

        self.cash_bal = 0
        self.pure_bal = 0
        self.pure_to_pure_description_entry = None
        self.pure_to_pure_description_label = None
        self.pure_to_cash_description_entry = None
        self.pure_to_cash_description_label = None
        self.cash_entry_description_entry = None
        self.cash_entry_description_label = None
        self.transactions_tree = None
        self.last_10_transactions_frame = None
        self.closing_actual_pure_balance = None
        self.closing_actual_cash_balance = None
        self.closing_pure_balance_label = None
        self.closing_cash_balance_label = None
        self.closing_balance_frame = None
        self.opening_balance_frame = None
        self.pure_weight = None
        self.pure_weight_label = None
        self.gross_weight = None
        self.gross_weight_label = None
        self.pure_to_pure_label = None
        self.weight_label = None
        self.pure_to_pure_frame = None
        self.weight = None
        self.amount = None
        self.amount_label = None
        self.purity_label = None
        self.purity = None
        self.gold_rate = None
        self.gold_rate_label = None
        self.pure_to_cash_label = None
        self.pure_to_cash_frame = None
        self.cash_received_entry = None
        self.cash_received_label = None
        self.cash_entry_frame = None
        self.entry_frame = None
        self.opening_cash_balance_label = None
        self.opening_actual_cash_balance = None
        self.opening_pure_balance_label = None
        self.opening_actual_pure_balance = None
        self.db = sqlite3.connect("shop.db", detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)
        self.cursor = self.db.cursor()
        self.root_window = tk.Toplevel(top_window)
        self.root_window.grab_set()
        self.root_window.title('CASH ENTRY')
        width = self.root_window.winfo_screenwidth()
        height = self.root_window.winfo_screenheight()
        self.root_window.geometry(str(width - 100) + "x" + str(height - 100))
        self.cursor.execute("SELECT * from GOLDSMITHS;")
        result = self.cursor.fetchall()
        self.goldsmith_list = {x[1]: x[0] for x in result}
        self.lst = [x[1] for x in result]
        self.date = tk.StringVar()
        # creating Combobox
        self.root_window.option_add("*TCombobox*Listbox*Font", self.font)
        self.top_frame = tk.Frame(self.root_window, borderwidth=15)
        self.date_label = tk.Label(self.top_frame, font=('TkHeadingFont', 15), text="Date (DD-MM-YY) : ")
        self.date_label.pack(expand=True, side="left", anchor="e")
        self.date_entry = tk.Entry(self.top_frame, textvariable=self.date, justify="left", font=('TkHeadingFont', 15))
        self.date_entry.delete(0, tk.END)
        # self.date_entry.insert(0, datetime.datetime.strftime(datetime.datetime.now(), "%d-%m-%Y"))
        self.date_entry.pack(expand=True, side="left", anchor="w")
        self.goldsmith_label = tk.Label(self.top_frame, font=('TkHeadingFont', 15), text="Goldsmith Name : ")
        self.goldsmith_label.pack(expand=True, side="left", anchor="e")
        self.goldsmith_combo_box = ttk.Combobox(self.top_frame, font=('TkHeadingFont', 15))
        self.goldsmith_combo_box['values'] = self.lst
        self.goldsmith_combo_box.bind("<<ComboboxSelected>>", self.display_all_fields)
        self.goldsmith_combo_box.bind('<KeyRelease>', self.update_goldsmith_list)
        self.goldsmith_combo_box.pack(side="left", expand=True, anchor="w")
        self.top_frame.pack(side="top", expand=False, fill="x")
        self.submit_button = tk.Button(self.top_frame, justify="center", text="Add Entry", font=('TkHeadingFont', 15),
                                       background="gold", command=partial(self.add_entry, None))

        self.root_window.bind("<Alt-Shift-Key>", lambda event: self.goldsmith_combo_box.focus_set())

        self.root_window.bind("<F1>", lambda event: self.date_entry.focus_set())

    def add_entry(self, _):
        date = self.date.get().strip()
        date_present = False
        if date:
            date_present = True
            try:
                date = datetime.datetime.strptime(date, "%d-%m-%y")
            except (Exception,):
                messagebox.showerror("Date Error", "Check Date", parent=self.root_window)
                return
        if self.pure_to_cash_description_entry.get().strip() != "" and self.gold_rate.get().strip() != "" and \
                self.weight.get().strip() != "" and self.amount.get().strip() != "":
            desc = self.pure_to_cash_description_entry.get().strip()
            gold_rate = self.gold_rate.get().strip()
            amount = self.amount.get().strip()
            weight = self.weight.get().strip()
            try:
                _ = int(gold_rate)
            except (Exception,):
                messagebox.showerror("P2C Error", "Check Gold Rate", parent=self.root_window)
                return
            if len(gold_rate) != 4:
                messagebox.showerror("P2C Error", "Check Gold Rate", parent=self.root_window)
                return

            try:
                _ = int(amount)
            except (Exception,):
                messagebox.showerror("P2C Error", "Check Amount", parent=self.root_window)
                return

            try:
                weight = round(float(weight), 3)
            except (Exception,):
                messagebox.showerror("P2C Error", "Check Gold Rate and Amount", parent=self.root_window)
                return

            confirm = messagebox.askyesno("Confirmation", "P2C - " + amount + " - " + str(gold_rate) + " - " + str(
                weight) + " - " + desc + " ? ", parent=self.root_window)
            if confirm:
                if date_present:
                    self.cursor.execute(
                        "INSERT INTO GOLDSMITH_TRANSACTIONS (DATE, GOLDSMITH_ID, DESCRIPTION, GOLD_RATE, AMOUNT, "
                        "PURE_WEIGHT, TYPE) VALUES ('" + str(
                            date) + "', " + str(
                            self.goldsmith_details[0]) + ", '" + desc + "', " + gold_rate + ", " + str(
                            amount) + ", -" + str(weight) + ", 'p2c')")
                else:
                    self.cursor.execute(
                        "INSERT INTO GOLDSMITH_TRANSACTIONS (GOLDSMITH_ID, DESCRIPTION, GOLD_RATE, AMOUNT, "
                        "PURE_WEIGHT, TYPE) VALUES (" + str(
                            self.goldsmith_details[0]) + ", '" + desc + "', " + gold_rate + ", " + str(
                            amount) + ", -" + str(weight) + ", 'p2c')")
            else:
                return
        else:
            if self.pure_to_cash_description_entry.get().strip() == "" and self.gold_rate.get().strip() == "" and \
                    self.weight.get().strip() == "" and self.amount.get().strip() == "":
                pass
            # do nothing
            else:
                messagebox.showerror("Pure to Cash Error", "All fields neither empty nor filled",
                                     parent=self.root_window)
                return

        if self.cash_entry_description_entry.get().strip() != "" and self.cash_received_entry.get().strip() != "":
            desc = self.cash_entry_description_entry.get().strip()
            amount = self.cash_received_entry.get().strip()
            try:
                _ = int(amount)
            except (Exception,):
                messagebox.showerror("Cash Error", "Check Amount", parent=self.root_window)
                return
            confirm = messagebox.askyesno("Confirmation", "Cash - " + amount + " - " + desc + " ? ",
                                          parent=self.root_window)
            if confirm:
                if date_present:
                    self.cursor.execute(
                        "INSERT INTO GOLDSMITH_TRANSACTIONS (DATE, GOLDSMITH_ID, AMOUNT, DESCRIPTION, TYPE) "
                        "VALUES ('" + str(
                            date) + "', " + str(
                            self.goldsmith_details[0]) + ", " + str(int(amount)) + ", '" + desc + "', 'cash')")
                else:
                    self.cursor.execute(
                        "INSERT INTO GOLDSMITH_TRANSACTIONS (GOLDSMITH_ID, AMOUNT, DESCRIPTION, TYPE) VALUES (" + str(
                            self.goldsmith_details[0]) + ", " + str(int(amount)) + ", '" + desc + "', 'cash')")

            else:
                return

        else:
            if self.cash_entry_description_entry.get().strip() == "" and self.cash_received_entry.get().strip() == "":
                pass
                # do nothing
            else:
                messagebox.showerror("Cash Error", "All fields neither empty nor filled", parent=self.root_window)
                return

        if self.pure_to_pure_description_entry.get().strip() != "" and self.gross_weight.get().strip() != "" and \
                self.purity.get().strip() != "" and self.pure_weight.get().strip() != "":
            desc = self.pure_to_pure_description_entry.get().strip()
            gross_wt = self.gross_weight.get().strip()
            purity = self.purity.get().strip()
            purity_ded = self.purity_ded.get().strip()
            pure_wt = self.pure_weight.get().strip()
            if purity_ded == "":
                messagebox.showerror("Pure to Pure Error", "Check Purity Ded",
                                     parent=self.root_window)
                return
            try:
                purity_ded = round(float(purity_ded), 2)
            except (Exception,):
                messagebox.showerror("Pure to Pure Error", "Check Purity Ded",
                                     parent=self.root_window)
                return
            try:
                gross_wt = round(float(gross_wt), 3)
            except (Exception,):
                messagebox.showerror("Pure to Pure Error", "Check Gross Weight",
                                     parent=self.root_window)
                return
            try:
                purity = round(float(purity), 2)
            except (Exception,):
                messagebox.showerror("Pure to Pure Error", "Check Purity",
                                     parent=self.root_window)
                return
            try:
                pure_wt = round(float(pure_wt), 3)
            except (Exception,):
                messagebox.showerror("Pure to Pure Error", "Check Gross Weight and Purity",
                                     parent=self.root_window)
                return
            confirm = messagebox.askyesno("Confirmation", "P2P - " + str(gross_wt) + " - " + str(purity) + " - " + str(
                pure_wt) + " - " + desc + " ? ", parent=self.root_window)
            if confirm:
                if date_present:
                    self.cursor.execute(
                        "INSERT INTO GOLDSMITH_TRANSACTIONS (DATE, GOLDSMITH_ID, DESCRIPTION, GROSS_WEIGHT, PURITY, "
                        "PURITY_DED, PURE_WEIGHT, TYPE) VALUES ('" + str(
                            date) + "', " + str(self.goldsmith_details[0]) + ", '" + desc + "', -" + str(
                            gross_wt) + ", " + str(
                            purity) + ", " + str(
                            purity_ded) + ", -" + str(pure_wt) + ", 'p2p')")
                else:
                    self.cursor.execute(
                        "INSERT INTO GOLDSMITH_TRANSACTIONS (GOLDSMITH_ID, DESCRIPTION, GROSS_WEIGHT, PURITY, "
                        "PURITY_DED, PURE_WEIGHT, TYPE) VALUES (" + str(
                            self.goldsmith_details[0]) + ", '" + desc + "', -" + str(gross_wt) + ", " + str(
                            purity) + ", " + str(
                            purity_ded) + ", -" + str(pure_wt) + ", 'p2p')")

        else:
            if self.pure_to_pure_description_entry.get().strip() == "" and self.gross_weight.get().strip() == "" \
                    and self.purity.get().strip() == "" and self.pure_weight.get().strip() == "":
                pass
            # do nothing
            else:
                messagebox.showerror("Pure to Pure Error", "All fields neither empty nor filled",
                                     parent=self.root_window)
                return
        self.cursor.execute("UPDATE GOLDSMITHS SET CASH_BAL = " + str(self.cash_bal) + ", PURE_BAL = " + str(
            round(self.pure_bal, 3)) + " WHERE ID = " + str(self.goldsmith_details[0]))
        self.db.commit()
        self.display_all_fields("")

    def display_all_fields(self, _):
        if self.last_10_transactions_frame is not None:
            self.last_10_transactions_frame.pack_forget()
            self.last_10_transactions_frame.destroy()
        if self.opening_balance_frame is not None:
            self.opening_balance_frame.pack_forget()
            self.opening_balance_frame.destroy()
        if self.entry_frame is not None:
            self.entry_frame.pack_forget()
            self.entry_frame.destroy()
        if self.closing_balance_frame is not None:
            self.closing_balance_frame.pack_forget()
            self.closing_balance_frame.destroy()
        if self.submit_button is not None:
            self.submit_button.pack_forget()

        self.cursor.execute("SELECT * FROM GOLDSMITHS WHERE NAME like '" + self.goldsmith_combo_box.get() + "'")
        self.goldsmith_details = self.cursor.fetchone()
        self.cash_bal = self.goldsmith_details[4]
        self.pure_bal = round(float(self.goldsmith_details[5]), 3)
        self.last_10_transactions_frame = tk.Frame(self.root_window, borderwidth=5, relief=tk.RAISED)
        self.last_10_transactions_frame.pack(side="top", expand=False, fill="x")
        self.transactions_tree = ttk.Treeview(self.last_10_transactions_frame)
        s = ttk.Style()
        s.theme_use('clam')
        s.configure('Treeview', rowheight=35)
        s.configure('Treeview.Heading', font=('TkHeadingFont', 15))
        self.transactions_tree.configure(columns=(
            "date", "type", "desc", "amount", "gold_rate", "gross_weight", "purity", "purity_ded", "pure_weight"),
            show="headings")
        self.transactions_tree.heading('date', text='Date')
        self.transactions_tree.heading('type', text='Type')
        self.transactions_tree.heading('desc', text='Description')
        self.transactions_tree.heading("amount", text='Amount')
        self.transactions_tree.heading("gold_rate", text="Gold Rate")
        self.transactions_tree.heading("gross_weight", text="Gross Weight")
        self.transactions_tree.heading("purity", text="Purity")
        self.transactions_tree.heading("purity_ded", text="Purity Minus")
        self.transactions_tree.heading("pure_weight", text="Pure Weight")

        col_width = self.transactions_tree.winfo_width() // 11

        self.cursor.execute(
            "SELECT date, type, description, amount, gold_rate, gross_weight, purity, purity_ded, pure_weight FROM "
            "GOLDSMITH_TRANSACTIONS WHERE GOLDSMITH_ID = " + str(
                self.goldsmith_details[0]) + " ORDER BY date DESC LIMIT 10")
        result = self.cursor.fetchall()
        values = []
        for i in result:
            row = []
            for val in i:
                if not row:
                    val = val + datetime.timedelta(hours=5, minutes=30)
                    row.append(val.strftime("%d-%m-%y %H:%M"))
                elif val is None:
                    row.append("-----")
                else:
                    row.append(val)
            values.append(row)

        for value in values:
            self.transactions_tree.insert('', tk.END, values=value, tags="columns")
        self.transactions_tree.tag_configure('columns', font=("TkHeadingFont", 15))
        self.transactions_tree.column('date', anchor="center", width=col_width * 2)
        self.transactions_tree.column('type', anchor="center", width=col_width * 1)
        self.transactions_tree.column('desc', anchor="center", width=col_width * 2)
        self.transactions_tree.column("amount", anchor="center", width=col_width * 1)
        self.transactions_tree.column("gold_rate", anchor="center", width=col_width * 1)
        self.transactions_tree.column("gross_weight", anchor="center", width=col_width * 1)
        self.transactions_tree.column("purity", anchor="center", width=col_width * 1)
        self.transactions_tree.column("purity_ded", anchor="center", width=col_width * 1)
        self.transactions_tree.column("pure_weight", anchor="center", width=col_width * 1)

        self.transactions_tree.pack(side="top", expand=True, fill="both")

        self.opening_balance_frame = tk.Frame(self.root_window, borderwidth=5, relief=tk.RAISED)
        self.opening_cash_balance_label = tk.Label(self.opening_balance_frame)
        self.opening_actual_cash_balance = tk.Label(self.opening_balance_frame)
        self.opening_pure_balance_label = tk.Label(self.opening_balance_frame)
        self.opening_actual_pure_balance = tk.Label(self.opening_balance_frame)
        self.opening_balance_frame.pack(side="top", expand=False, fill="x")
        self.opening_cash_balance_label.configure(justify='right', text='Cash Balance : ', anchor="e",
                                                  font=('TkHeadingFont', 15))
        self.opening_cash_balance_label.pack(expand=True, fill='x', side='left')
        self.opening_actual_cash_balance.configure(justify='left', text=self.goldsmith_details[4], anchor="w",
                                                   font=('TkHeadingFont', 15))
        self.opening_actual_cash_balance.pack(expand=True, fill='x', side='left')
        self.opening_pure_balance_label.configure(justify='right', text='Pure Balance : ', anchor="e",
                                                  font=('TkHeadingFont', 15))
        self.opening_pure_balance_label.pack(expand=True, fill='x', side='left')
        self.opening_actual_pure_balance.configure(justify='left', text=self.goldsmith_details[5], anchor="w",
                                                   font=('TkHeadingFont', 15))
        self.opening_actual_pure_balance.pack(expand=True, fill='x', side='left')

        self.entry_frame = tk.Frame(self.root_window)
        self.entry_frame.pack(expand=True, fill="both", side="top")

        self.cash_entry_frame = tk.Frame(self.entry_frame, borderwidth=5, relief=tk.RAISED)
        self.cash_entry_frame.pack(side="left", expand=True, fill="both")
        self.cash_entry_description_label = tk.Label(self.cash_entry_frame, justify="right", text="Entry Description "
                                                                                                  ": ",
                                                     font=('TkHeadingFont', 15))
        self.cash_entry_description_label.grid(row=1, column=0, sticky="ews")
        self.cash_entry_description_entry = tk.Entry(self.cash_entry_frame, justify="center",
                                                     font=('TkHeadingFont', 15))
        self.cash_entry_description_entry.grid(row=1, column=1, sticky="ws")

        self.cash_received_label = tk.Label(self.cash_entry_frame, justify="right", text="Amount : ",
                                            font=('TkHeadingFont', 15))
        self.cash_received_label.grid(row=2, column=0, sticky="ewn")
        self.cash_received_entry = tk.Entry(self.cash_entry_frame, justify="center", font=('TkHeadingFont', 15))
        self.cash_received_entry.grid(row=2, column=1, sticky="wn")
        self.cash_received_entry.bind('<KeyRelease>', self.update_cash_rec)
        self.cash_entry_frame.grid_rowconfigure('all', weight=1)
        self.cash_entry_frame.grid_columnconfigure('all', weight=1)

        self.pure_to_cash_frame = tk.Frame(self.entry_frame, borderwidth=5, relief=tk.RAISED)
        self.pure_to_cash_frame.pack(side="left", expand=True, fill="both")
        self.pure_to_cash_description_label = tk.Label(self.pure_to_cash_frame, justify="right",
                                                       text="Entry Description "
                                                            ": ", font=('TkHeadingFont', 15))
        self.pure_to_cash_description_label.grid(row=1, column=0, sticky="ew")
        self.pure_to_cash_description_entry = tk.Entry(self.pure_to_cash_frame, justify="center",
                                                       font=('TkHeadingFont', 15))
        self.pure_to_cash_description_entry.grid(row=1, column=1, sticky="w")
        self.pure_to_cash_label = tk.Label(self.pure_to_cash_frame, justify="center", background="gold",
                                           text="Enter Pure to Cash Details", font=('TkHeadingFont', 15))
        self.pure_to_cash_label.grid(row=0, column=0, columnspan=2)
        self.gold_rate_label = tk.Label(self.pure_to_cash_frame, justify="center", text="Gold Rate : ",
                                        font=('TkHeadingFont', 15))
        self.gold_rate_label.grid(row=2, column=0, sticky="ew")
        self.gold_rate = tk.Entry(self.pure_to_cash_frame, justify="center", font=('TkHeadingFont', 15))
        self.gold_rate.grid(row=2, column=1, sticky="w")
        self.gold_rate.bind('<KeyRelease>', self.track_gold_rate)

        self.weight_label = tk.Label(self.pure_to_cash_frame, justify="center", text="Weight :",
                                     font=('TkHeadingFont', 15))
        self.weight_label.grid(row=3, column=0, sticky="ew")

        self.weight = tk.Entry(self.pure_to_cash_frame, justify="center", font=('TkHeadingFont', 15))
        self.weight.grid(row=3, column=1, sticky="w")
        self.weight.bind('<KeyRelease>', self.track_weight)

        self.amount_label = tk.Label(self.pure_to_cash_frame, justify="center", text="Amount : ",
                                     font=('TkHeadingFont', 15))
        self.amount_label.grid(row=4, column=0, sticky="ew")
        self.amount = tk.Entry(self.pure_to_cash_frame, justify="center", font=('TkHeadingFont', 15))
        self.amount.grid(row=4, column=1, sticky="w")
        self.amount.bind('<KeyRelease>', self.track_amount)
        self.pure_to_cash_frame.grid_rowconfigure('all', weight=1)
        self.pure_to_cash_frame.grid_columnconfigure('all', weight=1)

        self.pure_to_pure_frame = tk.Frame(self.entry_frame, relief=tk.RAISED, borderwidth=5)
        self.pure_to_pure_frame.pack(side="left", expand=True, fill="both")
        self.pure_to_pure_description_label = tk.Label(self.pure_to_pure_frame, justify="right",
                                                       text="Entry Description "
                                                            ": ", font=('TkHeadingFont', 15))
        self.pure_to_pure_description_label.grid(row=1, column=0, sticky="ew")
        self.pure_to_pure_description_entry = tk.Entry(self.pure_to_pure_frame, justify="center",
                                                       font=('TkHeadingFont', 15))
        self.pure_to_pure_description_entry.grid(row=1, column=1, sticky="w")
        self.pure_to_pure_label = tk.Label(self.pure_to_pure_frame, justify="center", background="gold",
                                           text="Enter Pure to Pure Details", font=('TkHeadingFont', 15))
        self.pure_to_pure_label.grid(row=0, column=0, columnspan=2)

        self.gross_weight_label = tk.Label(self.pure_to_pure_frame, justify="center", text="Gross Weight : ",
                                           font=('TkHeadingFont', 15))
        self.gross_weight_label.grid(row=3, column=0, sticky="ew")
        self.gross_weight = tk.Entry(self.pure_to_pure_frame, justify="center", font=('TkHeadingFont', 15))
        self.gross_weight.grid(row=3, column=1, sticky="w")
        self.gross_weight.bind('<KeyRelease>', self.track_pure_weight)

        self.purity_label = tk.Label(self.pure_to_pure_frame, justify="center", text="Purity : ",
                                     font=('TkHeadingFont', 15))
        self.purity_label.grid(row=4, column=0, sticky="ew")
        self.purity = tk.Entry(self.pure_to_pure_frame, justify="center", font=('TkHeadingFont', 15))
        self.purity.grid(row=4, column=1, sticky="w")
        self.purity.bind('<KeyRelease>', self.track_pure_weight)

        self.purity_ded_label = tk.Label(self.pure_to_pure_frame, justify="center", text="Purity Minus: ",
                                         font=('TkHeadingFont', 15))
        self.purity_ded_label.grid(row=5, column=0, sticky="ew")
        self.purity_ded = tk.Entry(self.pure_to_pure_frame, justify="center", font=('TkHeadingFont', 15))
        self.purity_ded.grid(row=5, column=1, sticky="w")
        self.purity_ded.insert(0, "-0.00")
        self.purity_ded.bind('<KeyRelease>', self.track_pure_weight)

        self.pure_weight_label = tk.Label(self.pure_to_pure_frame, justify="center", text="Pure Weight : ",
                                          font=('TkHeadingFont', 15))
        self.pure_weight_label.grid(row=6, column=0, sticky="ew")
        self.pure_weight = tk.Entry(self.pure_to_pure_frame, justify="center", font=('TkHeadingFont', 15))
        self.pure_weight.grid(row=6, column=1, sticky="w")
        self.pure_to_pure_frame.grid_rowconfigure('all', weight=1)
        self.pure_to_pure_frame.grid_columnconfigure('all', weight=1)

        self.closing_balance_frame = tk.Frame(self.root_window, borderwidth=5, relief=tk.RAISED)
        self.closing_cash_balance_label = tk.Label(self.closing_balance_frame)
        self.closing_actual_cash_balance = tk.Label(self.closing_balance_frame)
        self.closing_pure_balance_label = tk.Label(self.closing_balance_frame)
        self.closing_actual_pure_balance = tk.Label(self.closing_balance_frame)
        self.closing_balance_frame.pack(side="top", expand=False, fill="x")
        self.closing_cash_balance_label.configure(justify='right', text='Cash Balance : ', anchor="e",
                                                  font=('TkHeadingFont', 15))
        self.closing_cash_balance_label.pack(expand=True, fill='x', side='left')
        self.closing_actual_cash_balance.configure(justify='left', text=str(self.cash_bal), anchor="w",
                                                   font=('TkHeadingFont', 15))
        self.closing_actual_cash_balance.pack(expand=True, fill='x', side='left')
        self.closing_pure_balance_label.configure(justify='right', text='Pure Balance : ', anchor="e",
                                                  font=('TkHeadingFont', 15))
        self.closing_pure_balance_label.pack(expand=True, fill='x', side='left')
        self.closing_actual_pure_balance.configure(justify='left', text=str(self.pure_bal), anchor="w",
                                                   font=('TkHeadingFont', 15))
        self.closing_actual_pure_balance.pack(expand=True, fill='x', side='left')

        self.submit_button.pack(side="left", expand=True)

        self.root_window.bind("<Control-a>", lambda event: self.cash_entry_description_entry.focus_set())
        self.root_window.bind("<Control-b>", lambda event: self.pure_to_cash_description_entry.focus_set())
        self.root_window.bind("<Control-c>", lambda event: self.pure_to_pure_description_entry.focus_set())
        self.root_window.bind("<Key-Return>", lambda event: self.add_entry(event))

    def track_gold_rate(self, _):
        self.amount.delete(0, tk.END)
        self.weight.delete(0, tk.END)

    def update_cash_rec(self, _):
        cash = self.cash_received_entry.get().strip()
        self.cash_bal = int(self.goldsmith_details[4])
        self.pure_bal = round(float(self.goldsmith_details[5]), 3)
        if cash != "":
            try:
                cash = int(cash)
                self.cash_bal += cash
                self.closing_actual_cash_balance.configure(text=str(self.cash_bal))
            except (Exception,):
                pass
        else:
            self.closing_actual_cash_balance.configure(text=str(self.goldsmith_details[4]))

        amount = self.amount.get().strip()
        if amount != "":
            try:
                amount = int(amount)
                self.cash_bal += amount
                self.closing_actual_cash_balance.configure(text=str(self.cash_bal))
            except (Exception,):
                pass

        weight = self.weight.get().strip()
        if weight != "":
            try:
                weight = round(float(weight), 3)
                self.pure_bal -= weight
                self.pure_bal = round(float(self.pure_bal), 3)
                self.closing_actual_pure_balance.configure(text=str(self.pure_bal))
            except (Exception,):
                pass
        else:
            self.closing_actual_pure_balance.configure(text=str(self.goldsmith_details[5]))

        pure_wt = self.pure_weight.get().strip()
        if pure_wt != "":
            try:
                pure_wt = round(float(pure_wt), 3)
                self.pure_bal -= pure_wt
                self.pure_bal = round(float(self.pure_bal), 3)
                self.closing_actual_pure_balance.configure(text=str(self.pure_bal))
            except (Exception,):
                pass

    def track_pure_weight(self, _):
        gross_wt = self.gross_weight.get().strip()
        purity = self.purity.get().strip()
        purity_ded = self.purity_ded.get().strip()

        if gross_wt == "" or purity == "" or purity_ded == "":
            self.pure_weight.delete(0, tk.END)
            self.update_cash_rec("")
            return
        try:
            gross_wt = round(float(gross_wt), 3)
            purity = round(float(purity), 2)
            purity_ded = round(float(purity_ded), 2)
            pure_wt = round(float(gross_wt * (purity - abs(purity_ded)) / 100), 3)
            self.pure_weight.delete(0, tk.END)
            self.pure_weight.insert(0, pure_wt)
        except (Exception,):
            self.pure_weight.delete(0, tk.END)
            self.update_cash_rec("")
            return
        self.update_cash_rec("")

    def track_amount(self, _):
        rate = self.gold_rate.get().strip()
        try:
            rate = int(rate)
        except (Exception,):
            self.weight.delete(0, tk.END)
            self.update_cash_rec("")
            return
        amount = self.amount.get().strip()
        try:
            amount = float(amount)
        except (Exception,):
            self.weight.delete(0, tk.END)
            self.update_cash_rec("")
            return
        self.weight.delete(0, tk.END)
        weight = round(float(amount / rate), 3)
        self.weight.insert(0, weight)
        self.update_cash_rec("")

    def track_weight(self, _):
        rate = self.gold_rate.get().strip()
        try:
            rate = int(rate)
        except (Exception,):
            self.amount.delete(0, tk.END)
            self.update_cash_rec("")
            return
        weight = self.weight.get().strip()
        try:
            weight = float(weight)
        except (Exception,):
            self.amount.delete(0, tk.END)
            self.update_cash_rec("")
            return
        self.amount.delete(0, tk.END)
        amount = int(weight * rate)
        self.amount.insert(0, amount)
        self.update_cash_rec("")

    def update_goldsmith_list(self, event):
        value = event.widget.get()

        if value == '':
            self.goldsmith_combo_box['values'] = self.lst
        else:
            data = []
            for item in self.lst:
                if value.lower() in item.lower():
                    data.append(item)

            self.goldsmith_combo_box['values'] = data


def add_purchase(main_window, _):
    purchase_entry_window = PurchaseEntry(main_window)
    purchase_entry_window.root_window.mainloop()
