DROP TABLE IF EXISTS "goldsmiths";
CREATE TABLE IF NOT EXISTS "goldsmiths"
(
 "id"       INTEGER PRIMARY KEY NOT NULL,
 "name"     varchar(50) NOT NULL,
 "code"     varchar(10) NOT NULL,
 "phone_no" varchar(50) NOT NULL,
 "cash_bal" int NOT NULL,
 "pure_bal" FLOAT NOT NULL
);


DROP TABLE IF EXISTS "goldsmith_transactions";
CREATE TABLE IF NOT EXISTS "goldsmith_transactions"
(
 "id"           INTEGER PRIMARY KEY,
 "goldsmith_id" INTEGER NOT NULL ,
 "amount"       INTEGER,
 "pure_weight"  FLOAT,
 "purity"       FLOAT,
 "gross_weight" FLOAT,
 "purity_ded"   FLOAT,
 "gold_rate"    INTEGER,
 "description"  varchar(100) NOT NULL,
 "type"         varchar(5) NOT NULL,
 "date"         timestamp NOT NULL default CURRENT_TIMESTAMP,
 FOREIGN KEY ("goldsmith_id") REFERENCES "goldsmiths" ("id") 
);
