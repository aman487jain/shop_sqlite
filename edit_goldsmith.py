import sqlite3
import tkinter as tk
from tkinter import messagebox


class EditGoldsmith:
    def __init__(self, top_window):
        self.goldsmith_phone_no = None
        self.goldsmith_c = None
        self.goldsmith_id = None
        self.goldsmith_name = None
        self.goldsmith_details = None
        self.print_bar_code_button = None
        self.phone_number_entry = None
        self.phone_number = None
        self.goldsmith_code_entry = None
        self.goldsmith_code = None
        self.goldsmith_name_tamil_entry = None
        self.goldsmith_name_tamil = None
        self.goldsmith_name_entry = None
        self.goldsmith_name_label = None
        self.left_frame = None

        self.db = sqlite3.connect("shop.db")

        self.cursor = self.db.cursor()
        self.root_window = tk.Toplevel(top_window)
        self.root_window.grab_set()
        self.root_window.title('EDIT GOLDSMITH')
        GLOBAL_WIDTH = self.root_window.winfo_screenwidth()
        GLOBAL_HEIGHT = self.root_window.winfo_screenheight()
        self.root_window.geometry(str(GLOBAL_WIDTH - 100) + "x" + str(GLOBAL_HEIGHT - 100))

        self.goldsmith_total_list = []
        self.left_most_frame = tk.Frame(self.root_window)
        self.search_goldsmith = tk.StringVar()
        self.search_goldsmith.trace('w', self.update_goldsmith_listbox)
        self.search_goldsmith_entry = tk.Entry(self.left_most_frame, textvariable=self.search_goldsmith,
                                               font=('TkHeadingFont', 25))
        self.search_goldsmith_entry.pack(expand=False, fill='x', side='top')

        self.goldsmith_list_box = tk.Listbox(self.left_most_frame, selectmode="single", exportselection=False,
                                             font=('TkHeadingFont', 25), selectbackground="gold",
                                             selectforeground="black")
        self.goldsmith_list_box.bind("<<ListboxSelect>>", self.display_goldsmith_details)

        self.cursor.execute("SELECT ID, NAME FROM GOLDSMITHS;")
        self.goldsmith_list = self.cursor.fetchall()

        for i in self.goldsmith_list:
            self.goldsmith_list_box.insert(i[0], i[1])

        self.goldsmith_list_box.pack(expand=True, fill='both', side='left')
        self.goldsmith_list_box.bind("<<ListboxSelect>>", self.display_goldsmith_details)
        self.goldsmith_list_scrollbar = tk.Scrollbar(self.left_most_frame)
        self.goldsmith_list_scrollbar.configure(orient='vertical', width=15)
        self.goldsmith_list_scrollbar.pack(expand=False, fill='y', side='left')
        # Attaching Listbox to Scrollbar
        # Since we need to have a vertical
        # scroll we use yscrollcommand
        self.goldsmith_list_box.config(yscrollcommand=self.goldsmith_list_scrollbar.set)

        # setting scrollbar command parameter
        # to listbox.yview method its yview because
        # we need to have a vertical view
        self.goldsmith_list_scrollbar.config(command=self.goldsmith_list_box.yview)
        self.left_most_frame.configure(borderwidth='10')
        self.left_most_frame.pack(expand=False, fill='y', side='left')

    def update_goldsmith_listbox(self, *_):
        search_term = self.search_goldsmith.get()
        self.goldsmith_list_box.delete(0, tk.END)
        if search_term.strip() == "":
            for item in self.goldsmith_list:
                self.goldsmith_list_box.insert(item[0], item[1])
        else:
            for item in self.goldsmith_list:
                if search_term.lower() in item[1].lower():
                    self.goldsmith_list_box.insert(item[0], item[1])

    def display_goldsmith_details(self, _):
        if self.left_frame is not None:
            self.left_frame.pack_forget()
            self.left_frame.destroy()

        self.cursor.execute("SELECT NAME, CODE, PHONE_NO, ID FROM GOLDSMITHS WHERE NAME LIKE '" +
                            self.goldsmith_list_box.get(self.goldsmith_list_box.curselection()[0]) + "'")
        self.goldsmith_details = self.cursor.fetchone()
        self.left_frame = tk.Frame(self.root_window)
        self.goldsmith_name_label = tk.Label(self.left_frame, font=('TkHeadingFont', 25))
        self.goldsmith_name_label.configure(justify='center', text='Enter Goldsmith Name')
        self.goldsmith_name_label.pack(expand=True, fill='both', side='top')
        self.goldsmith_name_entry = tk.Entry(self.left_frame, font=('TkHeadingFont', 25))
        self.goldsmith_name_entry.configure(justify='center')
        self.goldsmith_name_entry.delete(0, tk.END)
        self.goldsmith_name_entry.insert(0, self.goldsmith_details[0])
        self.goldsmith_name_entry.pack(expand=True, fill='both', side='top')

        self.goldsmith_code = tk.Label(self.left_frame, font=('TkHeadingFont', 25))
        self.goldsmith_code.configure(justify="center", text='Enter Goldsmith Code')
        self.goldsmith_code.pack(expand=True, fill='both', side='top')
        self.goldsmith_code_entry = tk.Entry(self.left_frame, justify='center', font=('TkHeadingFont', 25))
        self.goldsmith_code_entry.pack(expand=True, fill='both', side='top')
        self.goldsmith_code_entry.delete(0, tk.END)
        self.goldsmith_code_entry.insert(0, self.goldsmith_details[1])
        self.phone_number = tk.Label(self.left_frame, font=('TkHeadingFont', 25))
        self.phone_number.configure(justify="center", text='Enter Phone Number')
        self.phone_number.pack(expand=True, fill='both', side='top')
        self.phone_number_entry = tk.Entry(self.left_frame, justify='center', font=('TkHeadingFont', 25))
        self.phone_number_entry.delete(0, tk.END)
        self.phone_number_entry.insert(0, self.goldsmith_details[2])
        self.phone_number_entry.pack(expand=True, fill='both', side='top')
        self.print_bar_code_button = tk.Button(self.left_frame, justify="center", text="UPDATE",
                                               font=('TkHeadingFont', 35), command=self.update_goldsmith_details,
                                               background="gold")
        self.print_bar_code_button.pack(expand=True, fill="both", side="top")
        self.left_frame.configure(borderwidth='10', relief='raised')
        self.left_frame.pack(anchor='center', expand=True, fill='both', side='left')

    def update_goldsmith_details(self):
        self.goldsmith_name = self.goldsmith_name_entry.get().strip().upper()
        self.goldsmith_id = self.goldsmith_details[3]
        self.goldsmith_c = self.goldsmith_code_entry.get().strip().upper()
        self.goldsmith_phone_no = self.phone_number_entry.get().strip().upper()

        self.cursor.execute('''SELECT NAME, CODE, PHONE_NO, ID from GOLDSMITHS''')
        self.goldsmith_list = self.cursor.fetchall()

        if self.goldsmith_name is None or self.goldsmith_name.strip() == "":
            messagebox.showinfo("Name Error", "Goldsmith Name Empty", parent=self.root_window)
            return

        if self.goldsmith_name in [x[0] for x in self.goldsmith_list] and \
                self.goldsmith_name != self.goldsmith_details[0]:
            messagebox.showinfo("Name Error", "Goldsmith Already Present", parent=self.root_window)
            return

        if self.goldsmith_c is None or self.goldsmith_c == "":
            messagebox.showinfo("Code Error", "Code Empty", parent=self.root_window)
            return

        if self.goldsmith_c in [x[1] for x in self.goldsmith_list] and self.goldsmith_c != self.goldsmith_details[1]:
            messagebox.showinfo("Code Error", "Code Already Present", parent=self.root_window)
            return

        if self.goldsmith_phone_no is None or self.goldsmith_phone_no == "":
            messagebox.showinfo("Phone Error", "Phone Empty", parent=self.root_window)
            return

        try:
            _ = int(self.goldsmith_phone_no)
        except (Exception,):
            messagebox.showinfo("Phone Error", "Not a Valid Phone Number", parent=self.root_window)
            return

        if len(self.goldsmith_phone_no) != 10:
            messagebox.showinfo("Phone Error", "Phone Number not 10 Numbers", parent=self.root_window)
            return

        if self.goldsmith_phone_no in [x[2] for x in self.goldsmith_list] and \
                self.goldsmith_phone_no != self.goldsmith_details[2]:
            messagebox.showinfo("Phone Error", "Phone Already Present", parent=self.root_window)
            return

        command = "UPDATE GOLDSMITHS SET NAME = '" + str(self.goldsmith_name) + "', CODE = '" + \
                  str(self.goldsmith_c) + "', PHONE_NO = '" + str(self.goldsmith_phone_no) + "' WHERE ID = " + \
                  str(self.goldsmith_id)

        try:
            self.cursor.execute(command)
            self.db.commit()
            self.goldsmith_name_entry.delete(0, tk.END)
            self.goldsmith_code_entry.delete(0, tk.END)
            self.phone_number_entry.delete(0, tk.END)
            self.goldsmith_list_box.selection_clear(0, tk.END)
        except (Exception,) as _:
            messagebox.showerror("Error", "Unexpected error occurred...", parent=self.root_window)
            return


def edit_goldsmith(main_window, _):
    edit_goldsmith_window = EditGoldsmith(main_window)
    edit_goldsmith_window.root_window.mainloop()
