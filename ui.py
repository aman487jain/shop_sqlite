import tkinter as tk
from functools import partial
import sqlite3

import add_goldsmith
import add_purchase_entry
import edit_goldsmith
import view_goldsmith_transactions


class HomeScreen:
    def __init__(self):
        # build ui
        # create main window
        # establishing the connection

        self.goldsmith_menu = None

        self.db = sqlite3.connect("shop.db")

        self.cursor = self.db.cursor()

        self.root_window = tk.Tk()
        self.root_window.title("Seeman Jewellery")

        # fetch width
        width = self.root_window.winfo_screenwidth()
        # fetch height
        height = self.root_window.winfo_screenheight()

        # set screen size as full screen width - 100 and full screen height - 100
        self.root_window.geometry(str(width - 100) + "x" + str(height - 100))
        self.root_window.minsize(1000, 600)

        self.root_window.bind("<Control-g>", lambda event: add_goldsmith.add_goldsmith(self.root_window, event))
        self.root_window.bind("<Control-e>", lambda event: edit_goldsmith.edit_goldsmith(self.root_window, event))
        self.root_window.bind("<Control-p>", lambda event: add_purchase_entry.add_purchase(self.root_window, event))
        self.root_window.bind("<Control-t>",
                              lambda event: view_goldsmith_transactions.view_goldsmith_transactions(self.root_window,
                                                                                                    event))

        # create menubar
        self.menu_bar = tk.Menu(self.root_window, background='gold', foreground='black', activebackground='white',
                                activeforeground='black', font=("TkHeadingFont", 14), borderwidth=10)
        # get the menu bar ready using the create_menubar function
        self.create_menubar()
        # add menu_bar to
        self.root_window.config(menu=self.menu_bar)

    def create_menubar(self):
        self.menu_bar.delete(0, tk.END)

        self.goldsmith_menu = tk.Menu(self.menu_bar, tearoff=0, borderwidth=5, font=("TkHeadingFont", 14))
        self.goldsmith_menu.add_command(label="Purchase Entry (Ctrl+P)",
                                        command=partial(add_purchase_entry.add_purchase, self.root_window, None))

        self.goldsmith_menu.add_separator()
        self.goldsmith_menu.add_command(label="View Transactions (Ctrl+T)",
                                        command=partial(view_goldsmith_transactions.view_goldsmith_transactions,
                                                        self.root_window, None))
        self.goldsmith_menu.add_separator()
        self.goldsmith_menu.add_command(label="Add Goldsmith (Ctrl+G)",
                                        command=partial(add_goldsmith.add_goldsmith, self.root_window, None))
        self.goldsmith_menu.add_command(label="Edit Goldsmith Details (Ctrl+E)",
                                        command=partial(edit_goldsmith.edit_goldsmith, self.root_window, None))

        self.menu_bar.add_cascade(label="Goldsmiths", menu=self.goldsmith_menu)

    def run(self):
        self.root_window.mainloop()


if __name__ == '__main__':
    app = HomeScreen()
    app.run()
