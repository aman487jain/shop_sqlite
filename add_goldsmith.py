import sqlite3
import tkinter as tk
from tkinter import messagebox


class AddGoldsmith:
    def __init__(self, top_window):

        self.goldsmith_name = None
        self.goldsmith_id = None
        self.goldsmith_c = None
        self.goldsmith_phone_no = None
        self.goldsmith_list = []

        self.db = sqlite3.connect("shop.db")

        self.cursor = self.db.cursor()

        self.root_window = tk.Toplevel(top_window)
        self.root_window.grab_set()
        self.root_window.title('ADD GOLDSMITH')
        GLOBAL_WIDTH = self.root_window.winfo_screenwidth()
        GLOBAL_HEIGHT = self.root_window.winfo_screenheight()
        self.root_window.geometry(str(GLOBAL_WIDTH - 100) + "x" + str(GLOBAL_HEIGHT - 100))

        self.left_frame = tk.Frame(self.root_window)
        self.goldsmith_name_label = tk.Label(self.left_frame)
        self.goldsmith_name_label.configure(justify='center', text='Enter Goldsmith Name', font=('TkHeadingFont', 25))
        self.goldsmith_name_label.pack(expand=True, fill='both', side='top')
        self.goldsmith_name_entry = tk.Entry(self.left_frame, font=('TkHeadingFont', 25))
        self.goldsmith_name_entry.configure(justify='center')
        self.goldsmith_name_entry.pack(expand=True, fill='both', side='top')

        self.goldsmith_code = tk.Label(self.left_frame, font=('TkHeadingFont', 25))
        self.goldsmith_code.configure(justify="center", text='Enter Goldsmith Code')
        self.goldsmith_code.pack(expand=True, fill='both', side='top')
        self.goldsmith_code_entry = tk.Entry(self.left_frame, justify='center', font=('TkHeadingFont', 25))
        self.goldsmith_code_entry.pack(expand=True, fill='both', side='top')
        self.phone_number = tk.Label(self.left_frame, font=('TkHeadingFont', 25))
        self.phone_number.configure(justify="center", text='Enter Phone Number')
        self.phone_number.pack(expand=True, fill='both', side='top')
        self.phone_number_entry = tk.Entry(self.left_frame, justify='center', font=('TkHeadingFont', 25))
        self.phone_number_entry.pack(expand=True, fill='both', side='top')
        self.add_goldsmith_button = tk.Button(self.left_frame, font=('TkHeadingFont', 25), background="gold")
        self.add_goldsmith_button.configure(text='Add Goldsmith', command=self.check_goldsmith_exist_or_not)
        self.add_goldsmith_button.pack(expand=True, fill='both', side='top')
        self.left_frame.configure(borderwidth='10', relief='raised')
        self.left_frame.pack(anchor='center', expand=True, fill='both', side='left')

    def check_goldsmith_exist_or_not(self):
        self.goldsmith_name = self.goldsmith_name_entry.get().strip().upper()
        self.goldsmith_id = ""
        self.goldsmith_c = self.goldsmith_code_entry.get().strip().upper()
        self.goldsmith_phone_no = self.phone_number_entry.get().strip().upper()

        self.cursor.execute('''SELECT NAME, CODE, PHONE_NO, ID from GOLDSMITHS''')
        self.goldsmith_list = self.cursor.fetchall()

        if self.goldsmith_name is None or self.goldsmith_name.strip() == "":
            messagebox.showinfo("Name Error", "Goldsmith Name Empty", parent=self.root_window)
            return

        if self.goldsmith_name in [x[0] for x in self.goldsmith_list]:
            messagebox.showinfo("Name Error", "Goldsmith Already Present", parent=self.root_window)
            return

        if self.goldsmith_c is None or self.goldsmith_c == "":
            messagebox.showinfo("Code Error", "Code Empty", parent=self.root_window)
            return

        if self.goldsmith_c in [x[1] for x in self.goldsmith_list]:
            messagebox.showinfo("Code Error", "Code Already Present", parent=self.root_window)
            return

        if self.goldsmith_phone_no is None or self.goldsmith_phone_no == "":
            messagebox.showinfo("Phone Error", "Phone Empty", parent=self.root_window)
            return

        try:
            _ = int(self.goldsmith_phone_no)
        except (Exception,):
            messagebox.showinfo("Phone Error", "Not a Valid Phone Number", parent=self.root_window)
            return

        if len(self.goldsmith_phone_no) != 10:
            messagebox.showinfo("Phone Error", "Phone Number not 10 Numbers", parent=self.root_window)
            return

        if self.goldsmith_phone_no in [x[2] for x in self.goldsmith_list]:
            messagebox.showinfo("Phone Error", "Phone Already Present", parent=self.root_window)
            return

        command = "INSERT INTO GOLDSMITHS (NAME, CODE, PHONE_NO, CASH_BAL, PURE_BAL) VALUES ('" + \
                  self.goldsmith_name + "', '" + self.goldsmith_c + "', '" + self.goldsmith_phone_no + "', " + \
                  "0" + ", " + "0" + ");"

        try:
            self.cursor.execute(command)
            self.db.commit()
            self.goldsmith_name_entry.delete(0, tk.END)
            self.goldsmith_code_entry.delete(0, tk.END)
            self.phone_number_entry.delete(0, tk.END)
        except (Exception,) as _:
            messagebox.showerror("Error", "Unexpected error occurred...", parent=self.root_window)
            return


def add_goldsmith(main_window, _):
    add_goldsmith_window = AddGoldsmith(main_window)
    add_goldsmith_window.root_window.mainloop()
